Make sure you're using a git client, Sourcetree is reccomended.

# Compiling
To compile NgBot you'll need a copy of Visual Studio 2017 and .NET Framework 4.6.1 SDK installed. You can grab Visual Studio from here, the .NET framework SDK is included in the install: https://www.visualstudio.com/downloads/

To run the bot you will need to create a txt file next to the exe called token and paste it in there. The token to run the bot on the BallisticNG server is not provided.
