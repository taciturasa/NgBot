﻿using System;
using System.IO;
using System.Windows.Forms;

namespace NgBot
{
    public class Settings
    { 
        /// <summary>
        /// The token for logging in the bot.
        /// </summary>
        internal static string BotToken = "";
        /// <summary>
        /// The path to the txt file containing the bot token.
        /// </summary>
        internal static string TokenFilePath = Path.Combine(Application.StartupPath, "token.txt");

        /// <summary>
        /// Loads the settings for the bot.
        /// </summary>
        internal static void LoadSettings()
        {
            if (!File.Exists(TokenFilePath)) Console.WriteLine($"Couldn't load bot token! {TokenFilePath}");
            else BotToken = File.ReadAllText(TokenFilePath);
        }
    }
}
