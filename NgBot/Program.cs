﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace NgBot
{
    class Program
    {
        /// <summary>
        /// The client the bot is connected through.
        /// </summary>
        public static DiscordSocketClient Client;
        public static CommandService Commands;
        public static IServiceProvider Services;

        static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            try
            {
                Settings.LoadSettings();

                Client = new DiscordSocketClient();
                Commands = new CommandService();
                Services = new ServiceCollection()
                    .AddSingleton(Client)
                    .AddSingleton(Commands)
                    .BuildServiceProvider();

                Client.Log += Log;
                Client.GuildMemberUpdated += GuildUserUpdated;
                Client.Ready += Ready;

                await RegisterCommandsAsync();
                await Client.LoginAsync(TokenType.Bot, Settings.BotToken);
                await Client.StartAsync();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            await Task.Delay(-1);
        }

        private async Task Ready()
        {
            await EnumerateGameRole(Client, true);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task MessageRecieved(SocketMessage messagParam)
        {
            /*---Don't do anything if the message was a system message---*/
            SocketUserMessage message = messagParam as SocketUserMessage;
            if (message == null) return;
            if (message.Channel.Name != Database.TargetChannel) return;

            /*---Make sure the bot was mentioned---*/
            int argPos = 0;
            if (!message.HasMentionPrefix(Client.CurrentUser, ref argPos)) return;

            SocketCommandContext context = new SocketCommandContext(Client, message);
            IResult result = await Commands.ExecuteAsync(context, argPos, Services);
            if (!result.IsSuccess) await context.Channel.SendMessageAsync(result.ErrorReason);
        }


        private async Task GuildUserUpdated(SocketUser before, SocketUser after)
        {
            string gameBefore = before.Game?.Name ?? null;
            string gameAfter = after.Game?.Name ?? null;

            /*---User has started playing the game---*/
            if (gameBefore != Database.GameName && gameAfter == Database.GameName)
            {
                SocketGuild guild = ((SocketGuildUser) after).Guild;
                if (guild.Name != Database.ServerName) return;

                SocketRole gameRole = guild.Roles.FirstOrDefault(r => r.Name == Database.PlayingGameRole);
                if (gameRole == null) return;

                SocketGuildUser user = (SocketGuildUser)after;
                bool inRole = user.Roles.Contains(gameRole);
                if (!inRole) await user.AddRoleAsync(gameRole);
            }

            /*---User has stopped playing the game---*/
            if (gameBefore == Database.GameName && gameAfter != Database.GameName)
            {
                SocketGuild guild = ((SocketGuildUser)after).Guild;
                if (guild.Name != Database.ServerName) return;

                SocketRole gameRole = guild?.Roles.FirstOrDefault(r => r.Name == Database.PlayingGameRole);
                if (gameRole == null) return;

                SocketGuildUser user = (SocketGuildUser)before;
                bool inRole = user.Roles.Contains(gameRole);
                if (inRole) await user.RemoveRoleAsync(gameRole);
            }
        }

        private async Task RegisterCommandsAsync()
        {
            Client.MessageReceived += MessageRecieved;
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        #region Processing

        /// <summary>
        /// Runs through every user on the target server and updates their roles if they're playing Database.GameName. WARNING: DO NOT USE OFTEN!
        /// </summary>
        public static async Task EnumerateGameRole(DiscordSocketClient client, bool assignToRole)
        { 
            /*---Make sure the guild and game role exists---*/
            SocketGuild guild = client.Guilds.FirstOrDefault(g => g.Name == Database.GameName);
            SocketRole gameRole = guild?.Roles.FirstOrDefault(r => r.Name == Database.PlayingGameRole);
            if (gameRole == null) return;

            foreach (SocketGuildUser user in guild.Users)
            {
                /*---Check if the user is playing the game and is in the playing role---*/
                bool playingGame = user.Game?.Name == Database.GameName;
                bool inRole = user.Roles.Contains(gameRole);

                /*---Update User Role---*/
                if (!assignToRole || !playingGame && inRole) await user.RemoveRoleAsync(gameRole);
                else if (!inRole && playingGame) await user.AddRoleAsync(gameRole);
            }
        }

        #endregion
    }
}
